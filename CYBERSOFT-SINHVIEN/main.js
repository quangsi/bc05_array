var danhSachTheTr = document.querySelectorAll("#tblBody tr");

// trả về điểm của 1 thẻ tr bất kì
var layDiemTuTheTr = function (trTag) {
  var danhSachTd = trTag.querySelectorAll("td");
  return danhSachTd[3].innerHTML * 1;
};

var layTenTuTheTr = function (trTag) {
  var danhSachTd = trTag.querySelectorAll("td");
  return danhSachTd[2].innerHTML;
};
// tìm sinh viên giỏi nhất

var theTrLonNhat = danhSachTheTr[0];
var theTrNhoNhat = danhSachTheTr[0];
var soSinhVienGioi = 0;
var danhSachSinhVienLonHon5 = "";
for (var index = 0; index < danhSachTheTr.length; index++) {
  // theTrHienTai là thẻ tr hiện tại
  var theTrHienTai = danhSachTheTr[index];
  // diemHienTai  => điểm trong thẻ tr hiện tại
  var diemHienTai = layDiemTuTheTr(theTrHienTai);
  // diemLonNhat => điểm trong thẻ tr lớn nhất
  var diemLonNhat = layDiemTuTheTr(theTrLonNhat);

  var diemNhoNhat = layDiemTuTheTr(theTrNhoNhat);
  if (diemHienTai > diemLonNhat) {
    theTrLonNhat = theTrHienTai;
  }
  if (diemHienTai < diemNhoNhat) {
    theTrNhoNhat = theTrHienTai;
  }
  if (diemHienTai >= 8) {
    soSinhVienGioi++;
  }
  if (diemHienTai >= 5) {
    var contentHTML = `
    <p class="text-danger"> ${layTenTuTheTr(theTrHienTai)} - ${layDiemTuTheTr(
      theTrHienTai
    )} </p>`;
    danhSachSinhVienLonHon5 += contentHTML;
  }
}

// good
// sinh viên giỏi nhất
document.getElementById("svGioiNhat").innerHTML = ` 
 ${layTenTuTheTr(theTrLonNhat)}   -  ${layDiemTuTheTr(theTrLonNhat)}`;
// sinh viên yếu nhất
document.getElementById("svYeuNhat").innerHTML = ` 
  ${layTenTuTheTr(theTrNhoNhat)}   -  ${layDiemTuTheTr(theTrNhoNhat)}`;
// số lượng sinh viên giỏi
document.getElementById("soSVGioi").innerHTML = soSinhVienGioi;
// dah sách
document.getElementById("dsDiemHon5").innerHTML = danhSachSinhVienLonHon5;
