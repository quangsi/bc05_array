// primitive type ~ string, number, boolean
// reference type ~ array, object

var menu = ["Bánh canh", "Bún bò", "Hủ tíu"];
console.log("menu: ", menu);

//  lấy số lượng hiện tại
var soLuong = menu.length;
console.log("soLuong: ", soLuong);

// thêm phần tử vào mảng
menu.push("Phở khô Gia Lai");
menu.push("Bún chả Hà Nội");
menu.push("Mì Quảng");
console.log("menu sau khi push: ", menu);

// duyện mảng
for (var index = 0; index < menu.length; index++) {
  var monAnHienTai = menu[index];
  // menu[index]  , index là vị trí của mỗi phần tử trong các lần lặp
  console.log("monAnHienTai: ", monAnHienTai);
}

// update giá trị của 1 phần tử trong array
menu[0] = "Bánh bèo";
console.log("menu sau khi update bánh bèo cho bánh canh: ", menu);

// CRUD
//  Create Read Update Delete

// forEach ~ duyệt mảng
var hienThiThongTin = function (monAn, index) {
  console.log("monAn - forEach: ", monAn, index);
};
menu.forEach(hienThiThongTin);

// pop

console.log("menu trc khi pop: ", menu);
menu.pop();
console.log("menu sau khi pop: ", menu);

var thongTinInput = "Bún bò";

//  indexOf trả về index đầu tiên nếu tìm thấy, ngược lại nếu kkhông tìm thấy thì trả về -1
var indexThongtin = menu.indexOf(thongTinInput);
console.log("indexThongtin: ", indexThongtin);
if (indexThongtin !== -1) {
  menu[indexThongtin] = "Bánh bao";
  console.log("menu sau khi update indexThongtin: ", menu);
}

// slice splice
console.log("menu: ", menu);
// vị trí, số lượng

let indexBanhBao = menu.indexOf("Bánh bao");
menu.splice(indexBanhBao, 1);

// map

var thucDon = menu.map(function (monAn) {
  return "món " + monAn;
});
console.log("menu: ", menu);
console.log("thucDon: ", thucDon);

var scores = [10, 10, 10, 1, 1, 1, 6, 7, 9, 9, 10, 10];

// filter
var resultScores = scores.filter(function (number) {
  return number == 10;
});

console.log("resultScores: ", resultScores);

console.log(`







`);
// tailwind
